
package org.yaukie.frame.test.service.impl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.yaukie.base.core.service.BaseService;
import org.springframework.stereotype.Service;
 import org.yaukie.frame.test.dao.mapper.DistLockMapper;
import org.yaukie.frame.test.model.DistLock;
import org.yaukie.frame.test.model.DistLockExample;
import org.yaukie.frame.test.service.api.DistLockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
        * @author: yuenbin
        * @create: 2022/12/26 16/20/318
        **/
        @Service
        @Slf4j
        public class DistLockServiceImpl   implements DistLockService {

        @Autowired
        private DistLockMapper distLockMapper;


                @Override
                public int doSth() {
                        doBefore();
                        log.info("执行某操作......");
                        doAfter();
                        return 0;

                }
                  @Transactional(propagation= Propagation.NOT_SUPPORTED,
                isolation = Isolation.READ_COMMITTED,
                rollbackFor = RuntimeException.class)
                   int doBefore() {
                         log.info("执行某操作之前......");
                        return 0;
                }

                private int doAfter() {
                        log.info("执行某操作之后......");
                        return 0;
                }

        @Override
        public long countByExample(DistLockExample example) {
                return 0;
        }

        @Override
        public int deleteByExample(DistLockExample example) {
                return 0;
        }

        @Override
        public int deleteByPrimaryKey(Integer id) {
                return 0;
        }

        @Override
        public int insert(DistLock record) {
                return 0;
        }

        @Override
        public int insertSelective(DistLock record) {
                return 0;
        }

        @Override
        public List<DistLock> selectByExample(DistLockExample example) {
                return null;
        }

        @Override
        public DistLock selectFirstExample(DistLockExample example) {
                return null;
        }

        @Override
        public DistLock selectByPrimaryKey(Integer id) {
                return null;
        }

        @Override
        public int updateByExampleSelective(DistLock record, DistLockExample example) {
                return 0;
        }

        @Override
        public int updateByExample(DistLock record, DistLockExample example) {
                return 0;
        }

        @Override
        public int updateByPrimaryKeySelective(DistLock record) {
                return 0;
        }

        @Override
        public int updateByPrimaryKey(DistLock record) {
                return 0;
        }
}
